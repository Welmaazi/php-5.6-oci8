ARG  PHP_VERSION
ARG  PHP_VERSION
FROM php:5.6-apache

LABEL maintainer.1="EL MAAZI walid  <walidelmaazi@gmail.com>"

# Customize any core extensions here
RUN apt-get update && apt-get install -y \
        git \
        unzip \
        iproute2 \
        libmcrypt-dev \
        libxml2-dev \
    && pecl install xdebug-2.5.5  \
    && docker-php-ext-enable xdebug  \
    && docker-php-ext-install -j$(nproc) iconv pdo pdo_mysql mcrypt soap mysqli \

    && curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \

    && ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/

# Add Xdebug configuration
ENV XDEBUGINI_PATH=/usr/local/etc/php/conf.d/xdebug.ini
RUN echo "zend_extension="`find /usr/local/lib/php/extensions/ -iname 'xdebug.so'` > $XDEBUGINI_PATH
COPY config/xdebug.ini /tmp/xdebug.ini
RUN cat /tmp/xdebug.ini >> $XDEBUGINI_PATH

COPY config/php.ini /usr/local/etc/php/
WORKDIR /var/www/html/

RUN apt-get update && apt install alien libaio1 -y

COPY ./oracle-rpms ./oracle-rpms
RUN alien -i ./oracle-rpms/* && rm -rf ./oracle-rpms

RUN pecl install oci8-2.0.12 --with-oci8=instantclient,/opt/oracle/instantclient \ 
 && docker-php-ext-enable oci8
EXPOSE 80 443
